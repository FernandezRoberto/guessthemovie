package org.fundacionjala.movie;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.nio.charset.Charset;

public class FileTest {
    @Test
    void fileReadsSpecificLine() {
        String result = "the matrix";
        
        String path = "src/test/resources/MovieTittles.txt"; 
        Charset charset = Charset.forName("UTF-8");
        FileReader files = new FileReader(charset);
        String container = files.readFile(path, 14);

        assertThat(container, equalTo(result));
    }

    @Test
    void fileReadsHowManyLinesItHas() {
        int result = 25;

        String path = "src/test/resources/MovieTittles.txt"; 
        Charset charset = Charset.forName("UTF-8");
        FileReader file = new FileReader(charset);
        int container = file.countFileLines(path);

        assertThat(container, equalTo(result));
    }

    @Test
    void fileReadsTrailingSpaces() {
        String result = "schindler's list";

        String path = "src/test/resources/MovieTittles.txt";
        Charset charset = Charset.forName("UTF-8");
        FileReader file = new FileReader(charset);
        String container = file.readFile(path, 4);

        assertThat(container, equalTo(result));
    }
}