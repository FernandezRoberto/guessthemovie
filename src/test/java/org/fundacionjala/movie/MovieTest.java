package org.fundacionjala.movie;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class MovieTest {
    @Test
    void getTheTittleMovie() {
        String result = "the matrix";
        
        Movie movie = new Movie("the matrix");
        String container = movie.getTittle();

        assertThat(container, equalTo(result));
    }

    @Test
    void setTheTittleMovie() {
        String result = "the matrix 2";
        
        Movie movie = new Movie("the matrix");
        movie.setTittle("the matrix 2");
        String container = movie.getTittle();

        assertThat(container, equalTo(result));
    }
}