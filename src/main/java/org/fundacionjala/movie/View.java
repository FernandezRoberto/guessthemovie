package org.fundacionjala.movie;

public class View {

    public void showStatus(String hiddenTittle, int guesses, String lettersGuessed) {
        System.out.println("You are guessing: " + hiddenTittle);
        System.out.println("You have guessed (" + guesses + ") wrong letter(s):" + lettersGuessed);
    }

    public void showInputMessage() {
        System.out.println("Guess a letter: ");
    }

    public void showGessedLetterMessage() {
        System.out.println("You already guessed that letter");
    }

    public void showWinScreen(String movieTittle) {
        System.out.println("YOU WIN!");
        System.out.println("You guessed " + movieTittle + " correctly.");
    }

    public void showLostScreen(String movieTittle) {
        System.out.println("YOU LOST!");
        System.out.println("The movie tittle was " + movieTittle);
    }

    public void showCategories() {
        System.out.println("---------------CATEGORIES---------------");
        System.out.println("  1.- Car models");
        System.out.println("  2.- Movies tittles");
        System.out.println("  3.- Profession names");
        System.out.println("  4.- Sport names");
    }

}