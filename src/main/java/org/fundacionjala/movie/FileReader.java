package org.fundacionjala.movie;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReader {
    
    final Charset charset;

    public FileReader(Charset charset) {
        this.charset = charset;
    }

    public int countFileLines(String path) {
        Path pathName = Paths.get(path);
        try (BufferedReader reader = Files.newBufferedReader(pathName, charset)) {
            int count = 0;
            while ((reader.readLine()) != null) {
                count++;
            }
            return count;
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
            return 0;
        }
    }

    public String readFile(String path, int lineNumber) {
        Path pathName = Paths.get(path);
        try (BufferedReader reader = Files.newBufferedReader(pathName, charset)) {
            String line;
            String word = "";
            int aux = 1;
            while ((line = reader.readLine()) != null) {
                if (lineNumber == aux) {
                    line = line.trim();
                    word = line + "\n";
                }
                aux++;
            }
            word = word.trim();
            return word.toLowerCase();
        } catch (IOException x) {
            System.err.format("IOException: %s%n", x);
            return null;
        }
    }
        
}