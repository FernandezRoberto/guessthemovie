package org.fundacionjala.movie;

public enum Category {
    CAR_MODELS,
    MOVIES,
    PROFESSIONS,
    SPORTS
}
