package org.fundacionjala.movie;

public class Word {

    String name;
    public Word (String name) {
        this.name = name.toLowerCase();
    }
    
    public boolean searchChar(String character){
        if (name.indexOf(character.toLowerCase()) > 0) {
            return true;

        } else {
            return false;

        }
    }

}