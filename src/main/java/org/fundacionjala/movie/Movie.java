package org.fundacionjala.movie;

import java.util.ArrayList;

import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class Movie {

    private String tittle;
    private Group group;
    private ArrayList <Label> letters;

    public Movie(String tittle) {
        this.tittle = tittle;
        letters = new ArrayList <Label>();
        group = new Group();
        initPublicTittle();

    }

    private void initPublicTittle(){
        double x = 120;
        for (int i = 0; i < tittle.length(); i++) {
            String character = tittle.charAt(i) == ' ' ? " " : "_";
            Label letter = new Label(character);
            letter.setTranslateX(x);
            letter.setTranslateY(600);
            letter.setFont(Font.font(40));
            x += 30;
            letters.add(letter);
            group.getChildren().add(letter);
        }

    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getTittle() {
        return tittle;
    }

    public String getHiddenTittle() {

        StringBuilder builder = new StringBuilder();

        for (int i = 0; i < tittle.length(); i++) {
            if (tittle.charAt(i) == ' ') {
                builder.append(" ");
            } else {
                builder.append(".");
            }
        }

        return builder.toString();
    }

    public boolean isHiddenTittle(String hiddenTittle) {
        return hiddenTittle.contains(".");
    }

    public boolean guessLetter(char letter) {
        return tittle.contains(String.valueOf(letter));
    }

    public void insertGuessedLetter(char letter) {

        group.getChildren().clear();
        for (int i = 0; i < tittle.length(); i++) {
            if (tittle.charAt(i) == letter) {
                letters.get(i).setText(""+letter);
            }
            group.getChildren().add(letters.get(i));
        }


    }

    public boolean letterAlreadyGuessed(char letter, String guessedLetters) {
        return guessedLetters.contains(String.valueOf(letter));
    }

    public Group getLetters() {
        return group;
    }
}