package org.fundacionjala.movie;

import java.nio.charset.Charset;
import java.util.Random;
import java.util.Scanner;

public class Controller {

    private final View view = new View();
    String movieTittle;
    Movie movie;

    public void startGame(int index){

        Scanner scan = new Scanner(System.in);

        final String charsetName = "UTF-8";
        final Category category = selectCategory(index);
        final String path = selectPath(category);

        Charset charset = Charset.forName(charsetName);
        FileReader fileReader = new FileReader(charset);
        int fileSize = fileReader.countFileLines(path);

        int randomNum = generateRandomNumber(fileSize);

        movieTittle = fileReader.readFile(path, randomNum);
        movie = new Movie(movieTittle);
        String hiddenTittle = movie.getHiddenTittle();

        int guesses = 0;
        String lettersGuessed = "";
        boolean winCondition = true;

        showFinalScreen(winCondition, movieTittle);
        scan.close();
    }

    public Movie getMovie() {
        return movie;
    }

    private String selectPath(Category category) {
        String path;

        switch (category) {
            case CAR_MODELS:
                path = "src/main/resources/CarModels.txt";
                break;
        
            case MOVIES:
                path = "src/main/resources/MovieTittles.txt";
                break;
        
            case PROFESSIONS:
                path = "src/main/resources/ProfessionNames.txt";
                break;
        
            case SPORTS:
                path = "src/main/resources/SportNames.txt";
                break;

            default:
                throw new IllegalArgumentException("This category does not exist");
        }

        return path;
    }

    private Category selectCategory(int index) {
        view.showCategories();

        Category category;
        String inputCategory = ""+index;
        switch (inputCategory) {
            case "1":
                category = Category.CAR_MODELS;
                break;
        
            case "2":
                category = Category.MOVIES;
                break;
        
            case "3":
                category = Category.PROFESSIONS;
                break;
        
            case "4":
                category = Category.SPORTS;
                break;
        
            default:
                category = Category.MOVIES;
                break;
        }

        return category;
    }

    private int generateRandomNumber(int fileSize) {
        Random rand = new Random();
        return rand.nextInt((fileSize - 0) + 1) + 0;
    }

    private void showFinalScreen(boolean winCondition, String movieTittle) {
        if (winCondition) {
            view.showWinScreen(movieTittle);
        } else {
            view.showLostScreen(movieTittle);
        }
    }

}