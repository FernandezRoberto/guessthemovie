package org.fundacionjala.movie;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class App extends Application {

    String category;
    Controller controller = new Controller();
    private StickMan man = new StickMan();

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        Scene scene = startGame(man);
        primaryStage.setTitle("imagen de ejemplo");
        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private Group initButtons(Movie movie, StickMan man){
        String[] abecedario = {"A","B","C","D","E","F","G","H","I","J",
        "K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
        double x = 100;
        double y = 500;
        Group group = new Group();
        for (int i = 0; i < abecedario.length; i++) {
            if(x == 400){
                x = 100;
                y += 30;
            }

            Buttons button = new Buttons(abecedario[i],man,movie,x,y);
            group.getChildren().add(button.group());
            x += 30;
        }
        return group;
        
    }

    private Scene startGame(StickMan man) {
        Label titulo = new Label("SELECCIONA UNA \nCATEGORIA DE PALABRAS");
        Group group = new Group();
        titulo.setTranslateX(100);
        titulo.setTranslateY(20);
        titulo.setFont(new Font("arial", 30));
        ListView<String> list = new ListView<>();
        list.getItems().addAll("Car models","Movies", "Professions", "Sports");
        list.setTranslateX(300);
        list.setTranslateY(150);
        list.setMaxSize(100, 150);
        list.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>(){
            @Override
            public void changed (ObservableValue<? extends String> x, String anterior, String actual){
                controller.startGame(list.getSelectionModel().getSelectedIndex()+1);
                System.out.println(list.getSelectionModel().getSelectedIndex());
                group.getChildren().clear();
                group.getChildren().add(initButtons(controller.getMovie(), man));
                group.getChildren().add(controller.getMovie().getLetters());
            }
        });
        
        group.getChildren().addAll(titulo, list);
        Scene scene = new Scene(group, 700, 700);
        return scene;
    }
    public static void main(String[] args) {
        launch(args);
    }

}
