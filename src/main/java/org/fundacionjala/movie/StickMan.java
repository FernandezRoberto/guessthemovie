package org.fundacionjala.movie;

import java.util.ArrayList;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
/**
 * StickMan
 */
public class StickMan {

    private ArrayList <Image> images;
    private String [] rutes;
    private int contador;
    private boolean lose;
    private ImageView imgView;

    public StickMan() {
        images = new ArrayList <Image>();
        rutes = new String[9];
        rutes[3] = "file:images\\Head.png";
        rutes[4] = "file:images\\Torso.png";
        rutes[5] = "file:images\\RightArm.png";
        rutes[6] = "file:images\\LeftArm.png";
        rutes[7] = "file:images\\RightLeg.png";
        rutes[8] = "file:images\\LeftLeg.png";
        rutes[0] = "file:images\\Gallow1.png";
        rutes[1] = "file:images\\Gallow2.png";
        rutes[2] = "file:images\\Gallow3.png";

        contador = 0;

        prepareParts();
    }

    public ImageView loadImage(){
        imgView = new ImageView();
        imgView.setImage(images.get(contador));

        imgView.setX(0);
        imgView.setY(100);
        contador++;
        imgView.setFitWidth(600);
        imgView.setFitHeight(300);
        lose = contador == 9 ? true : false;
        return imgView;
    }

    private void prepareParts () {
        for (int i = 0; i < rutes.length; i++) {
            images.add(new Image(rutes[i]));
        }
    }

    public boolean lose(){
        return lose;
    }

    

}