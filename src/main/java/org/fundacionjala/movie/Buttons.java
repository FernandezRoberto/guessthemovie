package org.fundacionjala.movie;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Buttons {
    Button button;
    boolean press;
    Group group;
    StickMan man;
    Movie movie;
    char name;

    public Buttons(String name, StickMan man, Movie movie, double x, double y) {
        button = new Button(name);
        button.setTranslateX(x);
        button.setTranslateY(y);
        group = new Group(button);
        this.man = man;
        press = false;
        startButton();
        this.movie = movie;
        this.name = name.toLowerCase().charAt(0);

    }

    public void startButton(){
        button.setOnAction(new EventHandler<ActionEvent>(){
        
            @Override
            public void handle(ActionEvent event) {
                if (movie.guessLetter(name)) {
                    movie.insertGuessedLetter(name);
                    group.getChildren().add(movie.getLetters());

                } else {
                    if (man.lose()) {
                        ImageView lose = new ImageView();
                        lose.setImage(new Image("file:images\\YOULOST.jpg"));
                        lose.setX(0);
                        lose.setY(0);
                        group.getChildren().add(lose);
                    } else{
                        group.getChildren().add(man.loadImage());
                    }

                }    

            }
            });
        }

    public Group group(){
        return group;
        
    }

    public Boolean getPress(){

        return press;
    }
}